# indie-token

> indie-token is an indieauth token endpoint.
>
> indie-token, is part of the indie-pen-dent project, a set of javascript/nodejs
> applications which implement the indie web standards.

[![pipeline status](https://gitlab.com/SChetwynd/indie-token/badges/master/pipeline.svg)](https://gitlab.com/SChetwynd/indie-token/-/commits/master)
[![coverage report](https://gitlab.com/SChetwynd/indie-token/badges/master/coverage.svg)](https://gitlab.com/SChetwynd/indie-token/-/commits/master)
[![Latest Release](https://gitlab.com/SChetwynd/indie-token/-/badges/release.svg)](https://gitlab.com/SChetwynd/indie-token/-/releases)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-e10079?logo=semantic-release)](https://github.com/semantic-release/semantic-release)

The main reasons behind this project:
- Increase number of token endpoints available.
- Make an easy to deploy token endpoint so anyone can deploy one.
- Add CORS to response to allow SPA's and PWA to auth.
- Expire tokens so they are not perpetually valid.

## Installation
TBD

## Usage
TBD

### Environment Variables
There only 1 variable required for indie-token to work

`JWT_KEY`

This is the key used to sign the Bearer tokens and must never be shared.

## Support
The best place for support will be on the [gitlab repository](https://gitlab.com/SChetwynd/indie-token/-/issues)
by submitting an issue.

## Roadmap
Currently this endpoint supports:
- Issuing token
- Verifying a token
- JSON content types
- x-www-form-urlencoded content types

Still to support
- `revoke` action
- Configurable token expiry
- Changes in working standard

## Contributing
I'm open to contributions, please submit issues in to the respository for ideas
to improve the project. All ideas are welcome and appreciated.

Any merge will be welcome and considered, but they must pass the current linting
rules, as well as the current tests, and ideally add more.

### Setting up for development
These steps will get a development setup running.

1. clone this repo,
2. create a `.env` file following the details above
3. run `pnpm i` (If installed, or checkout why its awesome here [https://pnpm.io/](https://pnpm.io/))
4. run `pnpm run dev`


## Authors and acknowledgment
It is currently a solo effort by [me](https://www.srchetwynd.co.uk), but any
help is welcome.

It would be unfair not to mention the [IndieAuth-Token-Endpoint](https://github.com/aaronpk/IndieAuth-Token-Endpoint)
and the [indieauth-client-php](https://github.com/indieweb/indieauth-client-php)
projects. Whilst this project is not a fork of those, it was heavily influenced
by them.

## License
This project is licensed under the [MIT License](https://mit-license.org/). A
copy of which should be included in this repository.

## Project status
This is currently under active development, but only in spare time around work,
and other commitments.
