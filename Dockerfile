FROM node:slim
WORKDIR /app
RUN npm i -g pnpm
COPY ./package.json ./package.json
RUN pnpm i

COPY ./tsconfig.json ./tsconfig.json
COPY ./src ./src
RUN pnpm run build
COPY ./public ./public

EXPOSE 3000
ENTRYPOINT ["pnpm", "run", "start"]

