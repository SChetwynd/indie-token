import test from 'ava';
import { getConfig } from './config.js';

test('JWT_KEY, errors when not provided', (t) => {
    t.plan(1);

    t.throws(() => getConfig({}), {
        message: 'Environment: variable JWT_KEY, is not set. Not launching.',
    });
});

test('Should return JWT_KEY', (t) => {
    t.plan(1);

    const { jwtSecret } = getConfig({ JWT_KEY: 'test' });

    t.is(jwtSecret, 'test');
});

test('Should return default port', (t) => {
    t.plan(1);

    const { port } = getConfig({ JWT_KEY: 'test' });

    t.is(port, 3000);
});

test('Should return set port', (t) => {
    t.plan(1);

    const { port } = getConfig({ JWT_KEY: 'test', PORT: '1' });

    t.is(port, 1);
});
