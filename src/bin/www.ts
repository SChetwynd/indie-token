#!/usr/bin/env node

/**
 * Module dependencies.
 */
import app from '../app.js';
import debug from 'debug';
import http from 'node:http';
import process from 'node:process';
import { getConfig } from '../config.js';

const config = getConfig();

const logger = debug('indie-token:server');

/**
 * Get port from environment and store in Express.
 */

app.set('port', config.port);

/**
 * Create HTTP server.
 */

export const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(config.port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: Record<string, unknown>) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = 'Port ' + config.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            // eslint-disable-next-line no-console
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            // eslint-disable-next-line no-console
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    if (addr === null) {
        throw new Error('Addr is null');
    }
    const bind =
        typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    logger('Listening on ' + bind);
}
