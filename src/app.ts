import express from 'express';
import { Request, Response, Express, NextFunction } from 'express';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import { router } from './routes/routers.js';
import cors from 'cors';
import { CustomError } from './helpers/error.js';

const app: Express = express();

const __dirname = path.dirname(fileURLToPath(import.meta.url));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', router);
app.use('/', express.static('./public'));

// catch 404 and forward to error handler
app.use(function (_request, response, next) {
    response.redirect('/');
    return response.end();
});

// error handler
app.use(function (
    error: Error | CustomError,
    request: Request,
    response: Response,
    _next: NextFunction
) {
    if (app.get('env') === 'development') {
        // eslint-disable-next-line no-console
        console.log(error);
    }

    if (!('status' in error)) {
        response.status(500);
        return response.json({ error: 'Something unexpected went wrong.' });
    }

    response.status(error.status);
    response.statusMessage = error.message;

    if ('headers' in error && error.headers !== undefined) {
        for (const [header, value] of Object.entries(error.headers)) {
            response.setHeader(header, value);
        }
    }

    if ('body' in error && error.body !== undefined) {
        return response.json(error.body);
    }

    return response.end();
});

export default app;
