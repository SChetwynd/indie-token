import test from 'ava';
import { Response } from 'node-fetch';
import { IndieAuth } from './auth-endpoint.js';

const initialFetch = IndieAuth.fetch;

test.after.always(() => {
    IndieAuth.fetch = initialFetch;
});

function getHTMLWithLinks() {
    return `
        <html><head>
            <link rel="authorization_endpoint" href="https://indieauth.com/auth">
            <link rel="token_endpoint" href="https://token.indieauth.com/token">
            <link rel="revocation_endpoint" href="https://revocation.endpoint/">
            <link rel="introspection_endpoint" href="https://introspection.endpoint">
            <link rel="userinfo_endpoint" href="https://userinfo.endpoint">
            <link rel="micropub" href="https://micropub.endpoint">
            <link rel="microsub" href="https://microsub.endpoint">
        </head></html>
    `;
}

function getHeaders() {
    return {
        authorization_endpoint: 'https://indieauth.com/auth',
        token_endpoint: 'https://token.indieauth.com/token',
        revocation_endpoint: 'https://revocation.endpoint/',
        introspection_endpoint: 'https://introspection.endpoint',
        userinfo_endpoint: 'https://userinfo.endpoint',
        micropub: 'https://micropub.endpoint',
        microsub: 'https://microsub.endpoint',
        'indieauth-metadata': 'https://metadata.endpoint',
    };
}

test('IndieAuth discoverEndpoint', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverEndpoint('authorization_endpoint');

    t.is(result, 'https://indieauth.com/auth');
});

test('IndieAuth discoverAuthorizationEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverAuthorizationEndpoint();

    t.is(result, 'https://indieauth.com/auth');
});

test('IndieAuth discoverTokenEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverTokenEndpoint();

    t.is(result, 'https://token.indieauth.com/token');
});

test('IndieAuth discoverRevocationEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverRevocationEndpoint();

    t.is(result, 'https://revocation.endpoint/');
});

test('IndieAuth discoverIntrospectionEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverIntrospectionEndpoint();

    t.is(result, 'https://introspection.endpoint');
});

test('IndieAuth discoverUserinfoEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverUserinfoEndpoint();

    t.is(result, 'https://userinfo.endpoint');
});

test('IndieAuth discoverMicropubEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverMicropubEndpoint();

    t.is(result, 'https://micropub.endpoint');
});

test('IndieAuth discoverMicrosubEndpoint, html', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(new Response(getHTMLWithLinks(), { status: 200 }))
        );

    const result = await indieAuth.discoverMicrosubEndpoint();

    t.is(result, 'https://microsub.endpoint');
});

test('IndieAuth discoverAuthorizationEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverAuthorizationEndpoint();

    t.is(result, 'https://indieauth.com/auth');
});

test('IndieAuth discoverTokenEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverTokenEndpoint();

    t.is(result, 'https://token.indieauth.com/token');
});

test('IndieAuth discoverRevocationEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverRevocationEndpoint();

    t.is(result, 'https://revocation.endpoint/');
});

test('IndieAuth discoverIntrospectionEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverIntrospectionEndpoint();

    t.is(result, 'https://introspection.endpoint');
});

test('IndieAuth discoverUserinfoEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverUserinfoEndpoint();

    t.is(result, 'https://userinfo.endpoint');
});

test('IndieAuth discoverMicropubEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverMicropubEndpoint();

    t.is(result, 'https://micropub.endpoint');
});

test('IndieAuth discoverMicrosubEndpoint, headers', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverMicrosubEndpoint();

    t.is(result, 'https://microsub.endpoint');
});

test('IndieAuth, should prefer headers to links', async (t) => {
    t.plan(1);
    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                            <link rel="authorization_endpoint" href="I should not get this value">
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverAuthorizationEndpoint();

    t.is(result, 'https://indieauth.com/auth');
});

test('IndieAuth, discoverMetadataEndpoint', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                            <link rel="authorization_endpoint" href="I should not get this value">
                        </head></html>
                    `,
                    {
                        status: 200,
                        headers: getHeaders(),
                    }
                )
            )
        );

    const result = await indieAuth.discoverMetadataEndpoint();

    t.is(result, 'https://metadata.endpoint');
});

test('IndieAuth, should return undefined if not found', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                            <link rel="authorization_endpoint" href="I should not get this value">
                        </head></html>
                    `,
                    {
                        status: 200,
                    }
                )
            )
        );

    const result = await indieAuth.discoverMetadataEndpoint();

    t.is(result, undefined);
});

test('IndieAuth, throw if it site returns a none 200', async (t) => {
    t.plan(1);

    const indieAuth = new IndieAuth('https://www.srchetwynd.co.uk');

    IndieAuth.fetch = () =>
        new Promise((resolve) =>
            resolve(
                new Response(
                    `
                        <html><head>
                            <link rel="authorization_endpoint" href="I should not get this value">
                        </head></html>
                    `,
                    {
                        status: 400,
                    }
                )
            )
        );

    await t.throwsAsync(() => indieAuth.discoverMetadataEndpoint(), {
        message: '"me" url returned a none 200 status.',
    });
});
