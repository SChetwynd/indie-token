export function formatResponse(
    data: Record<string, string>,
    format: 'application/json' | 'application/x-www-form-urlencode'
): Record<string, string> | string {
    if (format === 'application/x-www-form-urlencode') {
        return new URLSearchParams(Object.entries(data)).toString();
    }

    if (format === 'application/json') {
        return data;
    }

    throw new Error(`Unknown format: "${format}".`);
}
