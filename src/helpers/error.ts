import createHTTPError from 'http-errors';

interface ErrorDetails {
    body?: Record<string, unknown>;
    headers?: Record<string, string>;
}

export interface CustomError extends ErrorDetails {
    status: number;
    message: string;
}

export function createError(
    status: number,
    statusText: string,
    details: ErrorDetails
): createHTTPError.HttpError {
    return createHTTPError(status, statusText, details);
}
