import { load as cheerioLoad, CheerioAPI } from 'cheerio';
import fetch from 'node-fetch';

export class IndieAuth {
    private me: string;
    private _page: CheerioAPI | undefined;
    private _headers: Headers | undefined;

    static fetch = fetch;

    constructor(me: string) {
        this.me = me;
    }

    private async loadPage() {
        const response = await IndieAuth.fetch(this.me);
        this._headers = response.headers;

        if (response.status !== 200) {
            throw new Error('"me" url returned a none 200 status.');
        }

        this._page = cheerioLoad(await response.text());
    }

    private async getPage() {
        if (!this._page) {
            await this.loadPage();
        }

        return this._page;
    }

    private async getHeaders() {
        if (!this._headers) {
            await this.loadPage();
        }

        return this._headers;
    }

    async discoverEndpoint(endpoint: string): Promise<string | undefined> {
        const headers = await this.getHeaders();

        if (headers === undefined) {
            throw new Error('This should be unreachable');
        }

        const headerValue = headers.get(endpoint);
        if (headerValue) {
            return headerValue;
        }

        const page = await this.getPage();

        if (page === undefined) {
            throw new Error('This should be unreachable');
        }

        const endpointElement = page(`[rel="${endpoint}"]`);

        const elementHref = endpointElement.attr('href');
        if (!elementHref) {
            return;
        }

        return elementHref;
    }

    discoverAuthorizationEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('authorization_endpoint');
    }

    discoverTokenEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('token_endpoint');
    }

    discoverRevocationEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('revocation_endpoint');
    }

    discoverIntrospectionEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('introspection_endpoint');
    }

    discoverUserinfoEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('userinfo_endpoint');
    }

    discoverMicropubEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('micropub');
    }

    discoverMicrosubEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('microsub');
    }

    discoverMetadataEndpoint(): Promise<string | undefined> {
        return this.discoverEndpoint('indieauth-metadata');
    }
}
