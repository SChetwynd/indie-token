import test from 'ava';
import { formatResponse } from './response.js';

test('formtResponse, error on unknown format', (t) => {
    t.plan(1);

    t.throws(() => formatResponse({}, 'fail' as 'application/json'), {
        message: 'Unknown format: "fail".',
    });
});

test('formatResponse, json return data entered', (t) => {
    t.plan(1);
    const input = { this: 'is', a: 'test' };

    const result = formatResponse(input, 'application/json');

    t.is(result, input);
});

test('formatResponse, form', (t) => {
    t.plan(1);
    const input = { this: 'is', a: 'test' };

    const result = formatResponse(input, 'application/x-www-form-urlencode');

    t.is(result, 'this=is&a=test');
});
