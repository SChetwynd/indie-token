import { createError } from './error.js';

type SupportedContentType =
    | 'application/json'
    | 'application/x-www-form-urlencode';

type SupportedAction = undefined;

export function validateContentType(
    contentType: unknown
): SupportedContentType {
    if (typeof contentType !== 'string') {
        throw createError(400, 'invalid_request', {
            body: {
                error: 'invalid_request',
                error_description: 'Invalid Content-Type: none provided',
            },
        });
    }

    if (/application\/json/.test(contentType)) {
        return 'application/json';
    }

    if (/application\/x-www-form-urlencode/.test(contentType)) {
        return 'application/x-www-form-urlencode';
    }

    throw createError(400, 'invalid_request', {
        body: {
            error: 'invalid_request',
            error_description: `Invalid Content-Type: ${contentType}. Must be either "application/json" or "application/x-www-form-urlencode".`,
        },
    });
}

export function validateAction(action: unknown): SupportedAction {
    if (action === undefined) {
        return undefined;
    }

    if (action === 'revoke') {
        throw createError(422, 'invalid_request', {
            body: {
                error: 'invalid_request',
                error_description: `Action: "revoke" is not supported`,
            },
        });
    }

    throw createError(400, 'invalid_request', {
        body: {
            error: 'invalid_request',
            error_description: `Action: "${action}" is not a recognised action, must be undefined, or "revoke".`,
        },
    });
}

export function validateMe(me: unknown): string {
    if (!me) {
        throw createError(400, 'invalid_request', {
            body: {
                error: 'invalid_request',
                error_description: 'No "me" parameter was provided',
            },
        });
    }

    if (typeof me !== 'string') {
        throw createError(400, 'invalid_request', {
            body: {
                error: 'invalid_request',
                error_description: 'The "me" parameter provided was not valid',
            },
        });
    }

    try {
        new URL(me);
    } catch {
        throw createError(400, 'invalid_request', {
            body: {
                error: 'invalid_request',
                error_description: 'The "me" parameter provided was not valid',
            },
        });
    }

    return me;
}

export function validateAuthHeader(header: unknown): string {
    if (typeof header !== 'string') {
        throw createError(401, 'invalid_client', {
            body: {
                error: 'invalid_client',
                error_description:
                    'An access token is required. Send an HTTP Authorization ' +
                    "header such as 'Authorization: Bearer xxxxxx'",
            },
            headers: {
                'WWW-Authenticate': 'Bearer',
            },
        });
    }

    const [bearer, token] = header.split(' ');

    if (bearer.toLowerCase() !== 'bearer' || token === undefined) {
        throw createError(401, 'invalid_client', {
            body: {
                error: 'invalid_client',
                error_description: 'The token provided was malformed.',
            },
            headers: {
                'WWW-Authenticate': 'Bearer',
            },
        });
    }

    return token;
}
