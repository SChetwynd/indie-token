import test from 'ava';
import {
    validateContentType,
    validateAction,
    validateMe,
    validateAuthHeader,
} from './request-validation.js';

interface CustomError {
    status: number;
    message: string;
    body: Record<string, string>;
    headers: Record<string, string>;
}

test('validateContentType, throw on no type', (t) => {
    t.plan(4);

    try {
        // eslint-disable-next-line unicorn/no-useless-undefined
        validateContentType(undefined);
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 400);
        t.is(error_.body.error, 'invalid_request');
        t.is(
            error_.body.error_description,
            'Invalid Content-Type: none provided'
        );
    }
});

test('validateContentType, should return json', (t) => {
    t.plan(1);

    const result = validateContentType('application/json');

    t.is(result, 'application/json');
});

test('validateContentType, should return form url encode', (t) => {
    t.plan(1);

    const result = validateContentType('application/x-www-form-urlencode');

    t.is(result, 'application/x-www-form-urlencode');
});

test('validateContentType, should handle extra data on end of header', (t) => {
    t.plan(1);

    const result = validateContentType('application/json; charset=utf-8');

    t.is(result, 'application/json');
});

test('validateContentType, should handle extra data on end of header, json', (t) => {
    t.plan(1);

    const result = validateContentType(
        'application/x-www-form-urlencode; charset=utf-8'
    );

    t.is(result, 'application/x-www-form-urlencode');
});

test('validateContentType, should error on unknown Content-Type, form', (t) => {
    t.plan(4);

    try {
        validateContentType('fail');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.body.error, 'invalid_request');
        t.is(error_.status, 400);
        t.is(
            error_.body.error_description,
            'Invalid Content-Type: fail. Must be either "application/json" or "application/x-www-form-urlencode".'
        );
    }
});

test('validateAction, should error on revoke action', (t) => {
    t.plan(4);

    try {
        validateAction('revoke');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 422);
        t.is(error_.body.error, 'invalid_request');
        t.is(
            error_.body.error_description,
            'Action: "revoke" is not supported'
        );
    }
});

test('validateAction, should error on unknown action', (t) => {
    t.plan(4);

    try {
        validateAction('fail');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 400);
        t.is(error_.body.error, 'invalid_request');
        t.is(
            error_.body.error_description,
            'Action: "fail" is not a recognised action, must be undefined, or "revoke".'
        );
    }
});

test('validateAction, should return undefined', (t) => {
    t.plan(1);

    // eslint-disable-next-line unicorn/no-useless-undefined
    const result = validateAction(undefined);

    t.is(result, undefined);
});

test('validateMe, should error if missing', (t) => {
    t.plan(4);

    try {
        // eslint-disable-next-line unicorn/no-useless-undefined
        validateMe(undefined);
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 400);
        t.is(error_.body.error_description, 'No "me" parameter was provided');
        t.is(error_.body.error, 'invalid_request');
    }
});

test('validateMe, should error if me is not a string', (t) => {
    t.plan(4);

    try {
        validateMe(8);
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 400);
        t.is(
            error_.body.error_description,
            'The "me" parameter provided was not valid'
        );
        t.is(error_.body.error, 'invalid_request');
    }
});

test('validateMe, should error on invalid url', (t) => {
    t.plan(4);

    try {
        validateMe('fail');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.message, 'invalid_request');
        t.is(error_.status, 400);
        t.is(error_.body.error, 'invalid_request');
        t.is(
            error_.body.error_description,
            'The "me" parameter provided was not valid'
        );
    }
});

test('validateMe, should return the url', (t) => {
    t.plan(1);

    const result = validateMe('https://www.example.com');

    t.is(result, 'https://www.example.com');
});

test('validateAuthHeader, should error if missing header', (t) => {
    t.plan(5);

    try {
        // eslint-disable-next-line unicorn/no-useless-undefined
        validateAuthHeader(undefined);
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.status, 401);
        t.is(error_.message, 'invalid_client');
        t.is(error_.body.error, 'invalid_client');
        t.is(
            error_.body.error_description,
            'An access token is required. Send an HTTP Authorization ' +
                "header such as 'Authorization: Bearer xxxxxx'"
        );
        t.is(error_.headers['WWW-Authenticate'], 'Bearer');
    }
});

test('validateAuthHeader, should error if missing bearer', (t) => {
    t.plan(5);

    try {
        validateAuthHeader('beerer token');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.status, 401);
        t.is(error_.message, 'invalid_client');
        t.is(error_.body.error, 'invalid_client');
        t.is(
            error_.body.error_description,
            'The token provided was malformed.'
        );
        t.is(error_.headers['WWW-Authenticate'], 'Bearer');
    }
});

test('validateAuthHeader, should error if missing token', (t) => {
    t.plan(5);

    try {
        validateAuthHeader('bearer');
    } catch (error) {
        const error_ = error as CustomError;
        t.is(error_.status, 401);
        t.is(error_.message, 'invalid_client');
        t.is(error_.body.error, 'invalid_client');
        t.is(
            error_.body.error_description,
            'The token provided was malformed.'
        );
        t.is(error_.headers['WWW-Authenticate'], 'Bearer');
    }
});

test('validateAuthHeader, should return the token', (t) => {
    t.plan(1);

    const result = validateAuthHeader('bearer token');

    t.is(result, 'token');
});
