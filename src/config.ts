import process from 'node:process';
import 'dotenv/config';

export function getConfig(environment = process.env) {
    if (environment.JWT_KEY === undefined) {
        throw new Error(
            'Environment: variable JWT_KEY, is not set. Not launching.'
        );
    }

    return {
        jwtSecret: environment.JWT_KEY,
        port: Number(environment.PORT) || 3000,
        version: environment.npm_package_version || 'dev',
    };
}
