import { Router } from 'express';
import { createError } from '../helpers/error.js';
import jwt from 'jsonwebtoken';
import fetch from 'node-fetch';

import {
    validateContentType,
    validateAction,
    validateMe,
    validateAuthHeader,
} from '../helpers/request-validation.js';
import { formatResponse } from '../helpers/response.js';
import { IndieAuth } from '../helpers/auth-endpoint.js';
import { getConfig } from '../config.js';

const config = getConfig();

export const tokenRouter: Router = Router();

/**
 * @swagger
 * /v1/token:
 *   post:
 *     tags:
 *      - v1
 *     description: Token endpoint to verify an authorization code
 *     externalDocs:
 *       url: https://www.w3.org/TR/indieauth/#token-request
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *              - me
 *              - code
 *              - client_id
 *              - grant_type
 *              - redirect_uri
 *             properties:
 *               action:
 *                 type: string
 *                 enum:
 *                  - revoke
 *               me:
 *                 type: string
 *                 format: url
 *               code:
 *                 type: string
 *               client_id:
 *                 type: string
 *               grant_type:
 *                 type: string
 *                 enum:
 *                  - authorization_code
 *               redirect_uri:
 *                 type: string
 *                 format: uri
 *         application/x-www-form-urlencoded:
 *           schema:
 *             type: object
 *             required:
 *              - me
 *              - code
 *              - client_id
 *              - grant_type
 *              - redirect_uri
 *             properties:
 *               action:
 *                 type: string
 *                 enum:
 *                  - revoke
 *               me:
 *                 type: string
 *                 format: url
 *               code:
 *                 type: string
 *               client_id:
 *                 type: string
 *               grant_type:
 *                 type: string
 *                 enum:
 *                  - authorization_code
 *               redirect_uri:
 *                 type: string
 *                 format: uri
 *     responses:
 *       '200':
 *         description: Authorization code has been verified succesfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 access_token:
 *                   type: string
 *                 token_type:
 *                   type: string
 *                   enum:
 *                    - Bearer
 *                 scope:
 *                   type: string
 *                 me:
 *                   type: string
 *                   format: url
 *           application/x-www-form-urlencoded:
 *             schema:
 *               type: object
 *               properties:
 *                 access_token:
 *                   type: string
 *                 token_type:
 *                   type: string
 *                   enum:
 *                    - Bearer
 *                 scope:
 *                   type: string
 *                 me:
 *                   type: string
 *                   format: url
 *       '400':
 *         description: The request sent was invalid
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                 error_description:
 *                   type: string
 *       '422':
 *         description: Requested action is not supported
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                 error_description:
 *                   type: string
 */
tokenRouter.post('/v1/token', async (request, response, next) => {
    try {
        const contentType = validateContentType(request.get('Content-Type'));
        const action = validateAction(request.body.action);
        const me = validateMe(request.body.me);

        if (action === undefined) {
            const indieAuth = new IndieAuth(me);

            const authEndpoint =
                await indieAuth.discoverAuthorizationEndpoint();

            if (!authEndpoint) {
                throw createError(400, 'missing_authorization_endpoint', {
                    body: {
                        error: 'missing_authorization_endpoint',
                        error_description:
                            'No authorization endpoint was discovered',
                    },
                });
            }

            const authURL = new URL(authEndpoint);
            authURL.searchParams.set('code', request.body.code);
            authURL.searchParams.set('client_id', request.body.client_id);
            authURL.searchParams.set('redirect_uri', request.body.redirect_uri);

            const verificationResponse = await fetch(authURL.toString(), {
                method: 'POST',
            });

            if (verificationResponse.status !== 200) {
                throw createError(
                    verificationResponse.status,
                    verificationResponse.statusText,
                    {}
                );
            }

            const verificationBody =
                (await verificationResponse.json()) as Record<string, string>;

            const tokenContent = {
                me: verificationBody.me,
                issued_by: 'https://' + request.hostname + '/token',
                client_id: request.body.client_id,
                issued_at: Date.now(),
                scope: verificationBody.scope,
                nonce: Math.floor(Math.random() * 100),
            };

            const jwtToken = jwt.sign(tokenContent, config.jwtSecret, {
                expiresIn: '1d',
                issuer: 'https://' + request.hostname,
            });

            const tokenResponse = {
                access_token: jwtToken,
                token_type: 'Bearer',
                scope: verificationBody.scope,
                me: verificationBody.me,
            };

            const formatedResponse = formatResponse(tokenResponse, contentType);
            response.set('content-type', contentType);
            return response.send(formatedResponse);
        }

        const formatedResponse = formatResponse({}, contentType);
        response.set('content-type', contentType);

        return response.send(formatedResponse);
    } catch (error) {
        return next(error);
    }
});

/**
 * @swagger
 * /v1/token:
 *   get:
 *     tags:
 *      - v1
 *     description: Token endpoint to verify an authorization code
 *     externalDocs:
 *       url: https://www.w3.org/TR/indieauth/#access-token-verification
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Bearer token
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Token succesfully verified
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       '400':
 *         description: Something was wrong with the request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                 error_description:
 *                   type: string
 *       '401':
 *         description: Auth was not valid
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 *                 error_description:
 *                   type: string
 */
tokenRouter.get('/v1/token', (request, response, next) => {
    try {
        const authToken = validateAuthHeader(request.get('Authorization'));

        let tokenContents;

        try {
            tokenContents = jwt.verify(authToken, config.jwtSecret);
        } catch {
            throw createError(401, 'invalid_client', {
                body: {
                    error: 'invalid_client',
                    error_description:
                        'The provided token could not be verified, it may have expired.',
                },
            });
        }

        if (typeof tokenContents === 'string') {
            throw createError(401, 'invalid_client', {
                body: {
                    error: 'invalid_client',
                    error_description:
                        'Token contains a string, was expecting JSON object.',
                },
            });
        }

        return response.send(tokenContents);
    } catch (error) {
        return next(error);
    }
});
