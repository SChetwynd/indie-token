import { Router } from 'express';
import { tokenRouter } from './token-v1.js';
import { swaggerRouter } from './swagger.js';

export const router: Router = Router();

router.use(tokenRouter);
router.use(swaggerRouter);
