import { Router } from 'express';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
import { getConfig } from '../config.js';

export const swaggerRouter: Router = Router();

const config = getConfig();

const swaggerOptions = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Indie Token',
            description: 'An Indie Token implementation which supports CORS',
            license: {
                name: 'MIT License',
                url: 'https://mit-license.org/',
            },
            version: config.version,
        },
        externalDocs: {
            description: 'Find out more about Indie Auth',
            url: 'https://www.w3.org/TR/indieauth/',
        },
    },
    apis: ['./dist/routes/*.js'],
};

const openapiSpecification = swaggerJsdoc(swaggerOptions);

swaggerRouter.use('/api-docs', swaggerUi.serve);
swaggerRouter.use('/api-docs', swaggerUi.setup(openapiSpecification));
