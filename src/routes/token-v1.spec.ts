import test from 'ava';
import request from 'supertest';
import { server } from '../bin/www.js';
import { getConfig } from '../config.js';
import jwt from 'jsonwebtoken';

const config = getConfig();
const { post, get } = request(server);

function getStatusMessage(response: unknown): string {
    const castResponse = response as {
        res: {
            statusMessage: string;
        };
    };

    return castResponse.res.statusMessage;
}

test('POST, error on invalid content type', async (t) => {
    t.plan(3);

    const result = await post('/v1/token');

    t.is(result.status, 400);
    t.is(getStatusMessage(result), 'invalid_request');
    t.deepEqual(result.body, {
        error: 'invalid_request',
        error_description: 'Invalid Content-Type: none provided',
    });
});

test('POST, error on invalid action', async (t) => {
    t.plan(3);

    const result = await post('/v1/token').send({
        action: 'fail',
    });

    t.is(result.status, 400);
    t.is(getStatusMessage(result), 'invalid_request');
    t.deepEqual(result.body, {
        error: 'invalid_request',
        error_description:
            'Action: "fail" is not a recognised action, must be undefined, or "revoke".',
    });
});

test('POST, error on missing "me"', async (t) => {
    t.plan(3);

    const result = await post('/v1/token').send({});

    t.is(result.status, 400);
    t.is(getStatusMessage(result), 'invalid_request');
    t.deepEqual(result.body, {
        error: 'invalid_request',
        error_description: 'No "me" parameter was provided',
    });
});

test('GET, error on missing token', async (t) => {
    t.plan(4);

    const result = await get('/v1/token');

    t.is(result.status, 401);
    t.is(getStatusMessage(result), 'invalid_client');
    t.deepEqual(result.body, {
        error: 'invalid_client',
        error_description:
            'An access token is required. Send an HTTP Authorization ' +
            "header such as 'Authorization: Bearer xxxxxx'",
    });
    t.is(result.header['www-authenticate'], 'Bearer');
});

test('GET, error on invalid token', async (t) => {
    t.plan(3);

    const token = jwt.sign({ a: 'b' }, 'fail');

    const result = await get('/v1/token').set(
        'Authorization',
        `Bearer ${token}`
    );

    t.is(result.status, 401);
    t.is(getStatusMessage(result), 'invalid_client');
    t.deepEqual(result.body, {
        error: 'invalid_client',
        error_description:
            'The provided token could not be verified, it may have expired.',
    });
});

test('GET, error on token with string content', async (t) => {
    t.plan(3);

    const token = jwt.sign('"fail"', config.jwtSecret);

    const result = await get('/v1/token').set(
        'Authorization',
        `Bearer ${token}`
    );

    t.is(result.status, 401);
    t.is(getStatusMessage(result), 'invalid_client');
    t.deepEqual(result.body, {
        error: 'invalid_client',
        error_description:
            'Token contains a string, was expecting JSON object.',
    });
});

test('GET, return the contents of a valid token', async (t) => {
    t.plan(2);

    const token = jwt.sign({ a: 'b' }, config.jwtSecret);

    const result = await get('/v1/token').set(
        'Authorization',
        `Bearer ${token}`
    );

    t.is(result.status, 200);
    t.like(result.body, {
        a: 'b',
    });
});
